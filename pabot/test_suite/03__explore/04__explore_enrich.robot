*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags  Explore     Enrich

*** Test Cases ***
Open Enrich Page For Data Object
    [Documentation]    User can click on a searched data object to go to enrich page
    [Tags]
    Click Explore Tab
    Search For Assets   '${ENRICH_TEST_DATA}'
    Open Enrich Page for Data Object    ${ENRICH_TEST_DATA}
    [Teardown]      Go To Home Page

Add New Property To A Data Object
    [Documentation]    User can click on a searched data object to go to enrich page and add a new property
    [Tags]  Fatal
    Click Explore Tab
    Search For Assets   '${ENRICH_TEST_DATA}'
    Open Enrich Page for Data Object    ${ENRICH_TEST_DATA}
    Add New Property To Data Object     test      test-data
    Go To Home Page
    Click Explore Tab
    Search For Assets   '${ENRICH_TEST_DATA}'
    Open Enrich Page for Data Object    ${ENRICH_TEST_DATA}
    Attribute Should Be Added     test      test-data
    Delete Attribute      test      test-data
    [Teardown]      Go To Home Page

Update Description Of A Data Object
    [Documentation]    User can click on a searched data object, go to its enrich page and update the description
    [Tags]
    Click Explore Tab
    Search For Assets   '${ENRICH_TEST_DATA}'
    Open Enrich Page for Data Object    ${ENRICH_TEST_DATA}
    Update Property     DESCR   'This is a test description'
    Go To Home Page
    Click Explore Tab
    Search For Assets   '${ENRICH_TEST_DATA}'
    Open Enrich Page for Data Object    ${ENRICH_TEST_DATA}
    Check If Property Updated   DESCR   'This is a test description'
    [Teardown]      Go To Home Page