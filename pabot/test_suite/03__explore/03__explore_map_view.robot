*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags  Explore     Map

*** Test Cases ***
Search All In Explore Map
    [Documentation]    The user can search all and go to map view, should be able to see the nodes corresponding to each technology
    [Tags]
    Click Explore Tab
    Expand Filter     ${TYPE_OF_ASSET}
    Go To Map View
    Show Hidden
    Zoom Out Map
    Check For Nodes
    [Teardown]  Go To Home Page


#Multiple Facet In Explore Map
#    [Documentation]    The user can select filter on multpile facets and only those nodes should remain on the map view
#    [Tags]
#    Click Explore Tab
#    Expand Filter     ${TYPE_OF_ASSET}
#    Apply Filter      ${TYPE_OF_ASSET}  ${ASSET_FILTER}
#    Apply Filter      ${TYPE_OF_ASSET}  ${ASSET_FILTER_2}
#    Go To Map View
#    Show Hidden
#    Map Should Only Contain     ${ASSET_FILTER}     ${ASSET_FILTER_2}
#    [Teardown]  Go To Home Page