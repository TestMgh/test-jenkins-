*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags      Explore     Export

*** Testcases ***
Export Fuzzy Search Results
    [Documentation]  User should be able to export the results of a fuzzy search which returns more than 190K assets.
    ...              Input: A fuzzy search
    ...              Output: A csv containing all rows returned as a result of the search
    [Tags]   Fuzzy
    Click Explore Tab
    Search For Assets   BUS_NAME=~${BUS_NAME_SUBSTR}
    ${rows_returned}    Get Rows Returned
    Run Keyword If     ${rows_returned}==0    Fail
    Export Results  'Export Assets'    Export_Nodes
    Check If File Contains All Rows    Export_Nodes    ${rows_returned}
    Remove File     ${DOWNLOAD_LOCATION}/Export_Nodes.csv
    [Teardown]  Go To Home Page

Export Facet Filter Results
    [Documentation]  User should be able to export the results of facet filter which returns more than 40K assets.
    ...              Input: Filter on a facet
    ...              Output: A csv containing all rows returned as a result of the facet filter
    [Tags]   Facets
    Click Explore Tab
    Expand Filter   ${TYPE_OF_ASSET}
    Apply Filter    ${TYPE_OF_ASSET}    ${PACKAGE}
    ${rows_returned}    Get Rows Returned
    Run Keyword If     ${rows_returned}==0    Fail
    Export Results  'Export Assets'    Export_Nodes
    Check If File Contains All Rows    Export_Nodes    ${rows_returned}
    Remove File     ${DOWNLOAD_LOCATION}/Export_Nodes.csv
    [Teardown]  Go To Home Page
