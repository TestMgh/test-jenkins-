*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags  Explore     Favourite


*** Test Cases ***
The users can add an asset to favourites
    [Documentation]    Users can add a node to favourites
    [Tags]     Fatal
    Click Explore Tab
    Mark Favourite     1
    ${business_name}   Return Marked Favourite
    Check If Node Added To Favourites   ${business_name}
    Remove From Favourites   ${business_name}
    [Teardown]   Go To Home Page


The users can open a favourite
    [Documentation]    Users can add a node to favourites
    [Tags]
    Click Explore Tab
    Mark Favourite     1
    ${business_name}   Return Marked Favourite
    Check If Node Added To Favourites   ${business_name}
    Execute Favourite   ${business_name}
    [Teardown]   Go To Home Page

Sort Assets by Favourites
    [Documentation]    Users can sort based on favourites
    [Tags]
    Click Explore Tab
    Expand Filter   ${TECHNOLOGY}
    Apply Filter    ${TECHNOLOGY}   ${SQL_SERVER}
    ${business_names}   Mark Assets As Favourite    5
    Go To Home Page
    Click Explore Tab
    Sort By Favourites
    Check If Marked Favourite    ${business_names}     5
    [Teardown]   Go To Home Page