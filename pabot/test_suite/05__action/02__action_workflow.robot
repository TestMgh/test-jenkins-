*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags      Action  Workflow

*** Test Cases ***
Workflow Dashboard On Search All
    [Documentation]     The workflow should appear on Search All Context on the workflow tab under Action
                        ...  User can navigate to Enrich Page of a data asset, under workflow section add workflow type, workflow status and save
                        ...  Navigating to Action under the workflow tab, in the workflow type under the particular workflow status, count should be 1.
    Click Explore Tab
    Sort On Column      Workflow Status
    ${asset_name}=   Open Enrich Page for Data Object
    Expand Workflow
    Populate Workflow Section
    Go To Home Page
    Click Action Tab
    Go To Workflow Dashboard
    Check If Workflow Present      ${asset_name}     Under Review
    Mark Workflow Completed        ${asset_name}
    [Teardown]  Go To Home Page

Workflow Dashboard On Subcontext
    [Documentation]     The workflow should appear on Subcontext on the workflow tab under Action
                            ...  User can navigate to Enrich Page of a data asset, under workflow section add workflow type, workflow status and save.Filtering on the type of asset.
                            ...  Navigating to Action under the workflow tab, in the workflow type under the particular workflow status, count should be 1.
    Click Explore Tab
    Sort On Column      Workflow Status
    ${asset_type}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text      xpath=//*[@id="nodesTable"]/tbody/tr/td[5]
    ${asset_name}=   Open Enrich Page for Data Object
    Expand Workflow
    Populate Workflow Section
    Go To Home Page
    Click Action Tab
    Go To Workflow Dashboard
    Expand Filter   ${TYPE_OF_ASSET}
    Run Keyword If     '${asset_type}'!='${ASSET_FILTER_2}'     Apply Filter    ${TYPE_OF_ASSET}    ${ASSET_FILTER_2}   ELSE    Apply Filter    ${TYPE_OF_ASSET}    ${ASSET_FILTER}
    Workflow Should Not Be Present      ${asset_name}
    Run Keyword If     '${asset_type}'!='${ASSET_FILTER_2}'     Apply Filter    ${TYPE_OF_ASSET}    ${ASSET_FILTER_2}   ELSE    Apply Filter    ${TYPE_OF_ASSET}    ${ASSET_FILTER}
    Apply Filter    ${TYPE_OF_ASSET}    ${asset_type}
    Check If Workflow Present      ${asset_name}     Under Review
    Mark Workflow Completed        ${asset_name}
    [Teardown]     Go To Home Page

Workflow Dashboard On Fullcontext
    [Documentation]     The workflow should appear on Fullcontext on the workflow tab under Action
                            ...  User can navigate to Enrich Page of a data asset, under workflow section add workflow type, workflow status and save. Perform a fuzzy search.
                            ...  Navigating to Action under the workflow tab, in the workflow type under the particular workflow status, count should be 1.
    Click Explore Tab
    Search For Assets   ${PROPERTY_SEARCH_TERM}
    Sort On Column      Workflow Status
    ${asset_type}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text      xpath=//*[@id="nodesTable"]/tbody/tr/td[5]
    ${asset_name}=   Open Enrich Page for Data Object
    Expand Workflow
    Populate Workflow Section
    Go To Home Page
    Click Action Tab
    Go To Workflow Dashboard
    Search For Assets   ${PROPERTY_SEARCH_TERM}
    Check If Workflow Present      ${asset_name}     Under Review
    Mark Workflow Completed        ${asset_name}
    [Teardown]     Go To Home Page
