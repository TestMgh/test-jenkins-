*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library  DateTime
Library  String
Library  Collections
Library  customized_keywords/MultipleFilesUpload.py


*** Variables ***
${URL}          https://localhost:9443
${BROWSER}      Chrome
${USERNAME}     admin
${PASSWORD}     Alex12345
${DELAY}        4 seconds
${SHORT_DELAY}  4 seconds
${RETRY_INT_LOGIN}  3 minutes
${RETRY_INT_OP}     15 seconds
${ALEX_HOME}    ${URL}/home
${ALEX_LOGOUT}    ${URL}/logout
${BUS_NAME_SEARCH_TERM}    DEQ_TID
${TECHNOLOGY}       Technology
${TECHNOLOGY_COL_NO}    12
${TECHNOLOGY_FILTER}    Oracle
${TYPE_OF_ASSET}    Type of Asset
${DATA_FLOW}    Data Flow
${DATA_FLOW_COL_NO}    13
${ASSET_COL_NO}     5
${ASSET_FILTER}     Table
${ASSET_FILTER_2}   Column
${DOWNLOAD_LOCATION}    /Users/neepachakraborty/Downloads
${IMPORT_DIR}     /Users/neepachakraborty/Downloads/ReleaseSet/
${IMPORT_TEST_DIR}     /Users/neepachakraborty/Downloads/test_data_upload/
${PROPERTY_SEARCH_TERM}  table
${BUS_NAME_SUBSTR}  Customer
${ASSET_PROPERTY}  Business name
${ASSET_PROPERTY_VALUE}  Customer
${NEIGHBOUR_FOR_NODE}   gw_mdfa_prty_add
${NUM_OF_PAGES_ORACLE}    127
${CI_SEARCH_TERM}       Pack
${REL_TYPE_VAL}     Cust
${PART_OF}      PART_OF
${SQL_SERVER}   SqlServer
${SELENIUM_SPEED}       0.2s
${ENRICH_TEST_DATA}     GW_MDFA_PRTY_ADDR_DAT
${Package}      Column
${IMPORT_DELAY}     40s
${EXPLORE_SEARCH_NODE}     STG_PATIENT_ACC_HIST_PROC

# User Admin
${USER_FIRST_NAME}  dummy
${USER_LAST_NAME}   dummy
${USER_PASSWORD}    Qwerty1234
${USER_EMAIL}       dummy@alexsolutions.com.au
${USER_CONTACT}     0412346354
${ROLE_DESCR}       Testing Role