*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags  Search

*** Test Cases ***
Search Nodes Based On Business Name
    [Documentation]  Users can search for nods based on business names
    [Tags]     Low
    Click Explore Tab
    Search For Assets    BUS_NAME="${BUS_NAME_SEARCH_TERM}"
    Node With This Business Name Returned
    [Teardown]   Go To Home Page

Locked Context Search
    [Documentation]  Search should be done on the locked result set
    [Tags]      Locked  Fatal
    Click Explore Tab
    Search For Assets    ${PROPERTY_SEARCH_TERM}
    Wait For Table to Populate
    Expand Filter     ${TECHNOLOGY}
    ${sql_server_tab}    Get Number Of Records For A Facet Filter    ${TECHNOLOGY}     ${SQL_SERVER}
    Go To Home Page
    Click Explore Tab
    Expand Filter     ${TECHNOLOGY}
    Apply Filter      ${TECHNOLOGY}     ${SQL_SERVER}
    Wait For Table to Populate
    Search For Assets   ${PROPERTY_SEARCH_TERM}
    Wait For Table to Populate
    ${result_no_lock}   Get Rows Returned
    ${result_no_lock}   Convert To Integer  ${result_no_lock}
    Go To Home Page
    Click Explore Tab
    Expand Filter     ${TECHNOLOGY}
    Apply Filter      ${TECHNOLOGY}     ${SQL_SERVER}
    Wait For Table to Populate
    Lock Context
    Search For Assets   ${PROPERTY_SEARCH_TERM}
    Wait For Table to Populate
    Sleep   ${DELAY}
    Sleep   ${DELAY}
    ${result_lock}     Get Rows Returned
    ${result_lock}   Convert To Integer  ${result_lock}
    ${status}       Evaluate    ${result_lock}==${sql_server_tab}
    ${status}       Run Keyword If    ${status}==True     Evaluate    ${result_lock}<=${result_no_lock}      ELSE     Fail
    Run Keyword If     ${status}!=True      Fail
    [Teardown]   Go To Home Page

Type Ahead Should Be Lower Case
    [Documentation]  The auto suggestion on the search bar should return lower case strings
    [Tags]      TypeAhead
    Check For Type Ahead    ${ASSET_PROPERTY_VALUE}
    [Teardown]   Go To Home Page

Filter Assets By Single Facet Filter
    [Documentation]    Users can filter node assets using single facet filter
    [Tags]      Facets
    Click Explore Tab
    Expand Filter     ${TECHNOLOGY}
    Apply Filter      ${TECHNOLOGY}  ${TECHNOLOGY_FILTER}
    Nodes With The Specific Filter Returned     ${TECHNOLOGY_FILTER}    ${TECHNOLOGY_COL_NO}
    [Teardown]   Go To Home Page


Filter Assets By Multiple Facet Filter
    [Documentation]    Users can filter node assets using multiple facet filter
    [Tags]      Facets  Fatal
    Click Explore Tab
    Expand Filter     ${TECHNOLOGY}
    Apply Filter      ${TECHNOLOGY}  ${TECHNOLOGY_FILTER}
    Expand Filter     ${TYPE_OF_ASSET}
    Apply Filter      ${TYPE_OF_ASSET}  ${ASSET_FILTER}
    Nodes With The Specific Filter Returned     ${TECHNOLOGY_FILTER}    ${TECHNOLOGY_COL_NO}
    Nodes With The Specific Filter Returned     ${ASSET_FILTER}   ${ASSET_COL_NO}
    [Teardown]   Go To Home Page

Check For Nulls In The Facets
    [Documentation]    If Null is selected, null assets should be appearing
    [Tags]      Facets
    Click Explore Tab
    Expand Filter    ${DATA_FLOW}
    Apply Filter    ${DATA_FLOW}    Null
    Nulls Nodes Should Be Returned
    [Teardown]   Go To Home Page


Select and Unselect All On Facet Filters
    [Documentation]    Selecting All on facet filters should return same number of nodes as by unselecting all
    [Tags]      Facets  Fatal
    Click Explore Tab
    Go To Map View
    Show Hidden
    Expand Filter     ${TYPE_OF_ASSET}
    Apply Filter      ${TYPE_OF_ASSET}  All
    ${num_of_assets}    Get Element Count  xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='Type of Asset']/parent::div/parent::div//*/label[text()!='All']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="graph-view-svg"]//*/*[@class='node']/child::*
    ${num_of nodes}    Get Element Count   xpath=//*[@id="graph-view-svg"]//*/*[@class='node']/child::*
    Should Be Equal    ${num_of nodes}     ${num_of_assets}
    [Teardown]   Go To Home Page

Endurance Test
    [Tags]      Endurance
    [Timeout]   1 hours
    ${fail}=     Evaluate   0
    :FOR    ${i}    IN RANGE    200
    \   ${passed}=   Run Keyword And Return Status   Search Endurance Test   ${ENRICH_TEST_DATA}
    \   Go To Home Page
    \   Continue For Loop If    ${passed}
    \   ${fail}     Evaluate     ${fail}+1
    ${success}=  Evaluate   200-${fail}
    Log Many   Success:  ${success}
    Log Many   fail:  ${fail}
    [Teardown]  Go To Home Page