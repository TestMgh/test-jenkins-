*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags  Search


*** Test Cases ***
Fuzzy search on property value
    [Documentation]  Search on property value, all records returned should have the string somewhere on it's enrich page
    [Tags]      Fuzzy
    Click Explore Tab
    Search For Assets  ${PROPERTY_SEARCH_TERM}
    Check If Nodes Returned Have Searched Property  ${PROPERTY_SEARCH_TERM}
    [Teardown]   Go To Home Page


Fuzzy search using =~
    [Documentation]  Search with =~ should return all records having the string for the asset property
    [Tags]      Fuzzy
    Click Explore Tab
    Search For Assets  BUS_NAME=~${BUS_NAME_SUBSTR}
    Check If Property Contains      ${BUS_NAME_SUBSTR}  6
    [Teardown]   Go To Home Page


Case insensitive fuzzy search on a property value
    [Documentation]     Users can do a case insensitive fuzzy search on a property value
    [Tags]      Fuzzy   Fatal
    Click Explore Tab
    Search For Assets   SUBCLASS=~${CI_SEARCH_TERM}
    Check If Property Contains  ${CI_SEARCH_TERM}   5
    [Teardown]   Go To Home Page

Reexecute Fuzzy search
    [Documentation]  User should be able to re execute fuzzy search from favourites
    [Tags]      Fuzzy
    Search For Assets  BUS_NAME=~${BUS_NAME_SUBSTR}
    ${search_text}  Get Element Attribute   //*[@id="searchAutoComplete"]    value
    Go To Advanced Search
    Save Search     fuzzy_search_exec
    Go To Home Page
    Go To Favourites
    Execute Search     fuzzy_search_exec
    ${saved_search_text}  Get Element Attribute   //*[@id="searchAutoComplete"]    value
    Should Be Equal   ${search_text}      ${saved_search_text}
    Go To Favourites
    Delete Saved Search  fuzzy_search_exec
    [Teardown]   Go To Home Page


