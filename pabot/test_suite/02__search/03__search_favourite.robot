*** Settings ***
Resource    ../keywords.robot
Suite Setup      Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}  Open Alex
Suite Teardown   Close Application
Force Tags  Search

*** Test Cases ***
Save a fuzzy search
    [Documentation]  User should be able to save a search criteria
    [Tags]      Favourite
    Search For Assets  BUS_NAME=~${BUS_NAME_SUBSTR}
    ${search_text}  Get Element Attribute   //*[@id="searchAutoComplete"]    value
    Go To Advanced Search
    Save Search     fuzzy_search
    Go To Home Page
    Go To Favourites
    Delete Saved Search  fuzzy_search
    [Teardown]   Go To Home Page

Reexecute Fuzzy search
    [Documentation]  User should be able to re execute fuzzy search from favourites
    [Tags]      Favourite
    Search For Assets  BUS_NAME=~${BUS_NAME_SUBSTR}
    ${search_text}  Get Element Attribute   //*[@id="searchAutoComplete"]    value
    Go To Advanced Search
    Save Search     fuzzy_search
    Go To Home Page
    Go To Favourites
    Execute Search     fuzzy_search
    ${saved_search_text}  Get Element Attribute   //*[@id="searchAutoComplete"]    value
    Should Be Equal   ${search_text}      ${saved_search_text}
    Go To Favourites
    Delete Saved Search  fuzzy_search
    [Teardown]   Go To Home Page


Save the advanced search
    [Documentation]    Users can save a search criteria
    [Tags]      Favourite   Fatal
    Go To Advanced Search
    Populate Values In Asset Property Advanced Search  ${ASSET_PROPERTY}  ${ASSET_PROPERTY_VALUE}
    Add Relationship Type In Advanced Search    ${PART_OF}    ${REL_TYPE_VAL}
    Save Search  fuzzy_test
    Go To Home Page
    Go To Favourites
    Check For Saved Search  fuzzy_test
    Delete Saved Search  fuzzy_test
    [Teardown]   Go To Home Page

Execute saved advanced search
    [Documentation]    Users can execute a saved search
    [Tags]      Favourite
    Go To Advanced Search
    Populate Values In Asset Property Advanced Search  ${ASSET_PROPERTY}  ${ASSET_PROPERTY_VALUE}
    Add Relationship Type In Advanced Search    ${PART_OF}    ${REL_TYPE_VAL}
    ${search_text}  Get Element Attribute   //*[@id="searchAutoComplete"]    value
    Click Search
    Go To Advanced Search
    Save Search  fuzzy_test
    Go To Home Page
    Go To Favourites
    Execute Search     fuzzy_test
    ${saved_search_text}  Get Element Attribute   //*[@id="searchAutoComplete"]    value
    Should Be Equal   ${search_text}      ${saved_search_text}
    Go To Favourites
    Delete Saved Search  fuzzy_test
    [Teardown]  Go To Home Page
