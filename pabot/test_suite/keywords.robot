*** Settings ***
Resource    common_settings.robot

*** Keywords ***
Open Application
    Open browser    ${URL}    ${BROWSER}
    Run Keyword If  '${BROWSER}'=='headlesschrome'      Set Window Size     1800    1400
    Set Selenium Speed  ${SELENIUM_SPEED}

Login Page Should Be Open
    Title Should Be    ALEX

Go To Login Page
    Go To    ${URL}
    Login Page Should Be Open

Go To Home Page
    Go To    ${ALEX_HOME}
    Sleep    ${SHORT_DELAY}

Go To Favourites
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="appbar-wrapper"]/div/div[1]/div[3]/div[2]/button
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="appbar-wrapper"]/div/div[1]/div[3]/div[2]/button
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element  xpath=//*/div[@role='menu']//*/div[text()='Favourites']/parent::div
    Sleep   ${SHORT_DELAY}
    Set Focus To Element   xpath=//*/div[@role='menu']//*/div[text()='Favourites']/parent::div
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*/div[@role='menu']//*/div[text()='Favourites']/parent::div
    Sleep   ${SHORT_DELAY}

Input Username
    [Arguments]    ${username}
    Input Text    username    ${username}

Continue
    Click Button    submit

Input Password
    [Arguments]    ${password}
    Input Text    password    ${password}

Submit Credentials
    Click Button    submit

Welcome Page Should Be Open
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    id:searchAutoComplete
    Location Should Be    ${ALEX_HOME}
    Title Should Be    ALEX

Login
    Input Username    ${USERNAME}
    Continue
    Input Password    ${PASSWORD}
    Submit Credentials
    Welcome Page Should Be Open
    Sleep   ${SHORT_DELAY}

Open Alex
    Open Application
    Login

Log Out
    Go To    ${ALEX_LOGOUT}

Close Application
    Log Out
    Close All Browsers

Select Node
    [Arguments]    ${node}  ${asset_type}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element   xpath=//*[@id="nodesTable"]/tbody/tr[1]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodesTable"]/tbody/tr/td[text()='${asset_type}']/following-sibling::td[text()='${node}']/parent::tr/td/input

Select The First Node
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element   xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input

Select First Two Nodes
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element   xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[1]/input
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[1]/input

# Tabs clicking
Click Explore Tab
#    Click Element    xpath://button[.//text() = 'Explore']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath://*[@id="appbar-wrapper"]//*/button//*/span[text()='Explore']
    Wait For Table to Populate
#    Sleep   ${SHORT_DELAY}

Click Capture Tab
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath://button[.//text() = 'Capture']

Click Action Tab
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath://*[@id="appbar-wrapper"]//*/button//*/span[text()='Action']

# Search nodes based on business names
Search For Assets
    [Arguments]    ${search_term}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text    id:searchAutoComplete    ${search_term}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath: //*[@id="appbar-wrapper"]/div/div[1]/div[2]/div[2]/button[1]

Node With This Business Name Returned
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="nodesTable"]/tbody/tr/td[6][contains(text(),'${BUS_NAME_SEARCH_TERM}')]
    Element Text Should Be    xpath=//*[@id="nodesTable"]/tbody/tr/td[6]    ${BUS_NAME_SEARCH_TERM}

# Facet filter
Expand Filter
    [Arguments]    ${filter_type}
#    Wait Until Page Contains Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='${filter_type}']/following-sibling::button
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='${filter_type}']/following-sibling::button
    Sleep  ${DELAY}

Apply Filter
    [Arguments]    ${filter_type}   ${filter}
#    Wait Until Page Contains Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]//*/div[text()='${filter_type}']/parent::div/following-sibling::div//label[contains(text(),'${filter}')]/parent::div/parent::div
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]//*/div[text()='${filter_type}']/parent::div/following-sibling::div//label[contains(text(),'${filter}')]/parent::div/parent::div
    Sleep  ${DELAY}

Nodes With The Specific Filter Returned
    [Arguments]    ${filter}    ${column_num}
    ${Rows}    Get Element Count    //*[@id="nodesTable"]/tbody/tr
    :FOR    ${row}    IN RANGE    1    ${Rows}
    \    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[${row}]/td[${column_num}][contains(text(),'${filter}')]

Nulls Nodes Should Be Returned
    ${Rows}    Get Element Count    //*[@id="nodesTable"]/tbody/tr
    :FOR    ${row}    IN RANGE    1    ${Rows}
    \    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="nodesTable"]/tbody/tr[${row}]/td[13][contains(text(),'')]


Go To Map View
     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="tab-content-switch-bar"]/div/div[1]//div[contains(text(),"Map")]
     Set Window Size     1700    1400
     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="tab-content-switch-bar"]/div/div[1]//div[contains(text(),"Map")]/parent::div
     Sleep  ${DELAY}

Show Hidden
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="graphExplorerControl"]/div/div[5]/button
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Button    xpath=//*[@id="graphExplorerControl"]/div/div[5]/button
#    Wait Until Page Contains Element     xpath=/html/body//*[contains(text(),'Show Hidden')]/parent::div/parent::div
#    Sleep  ${SHORT_DELAY}
#    Click Element    xpath=/html/body//*[contains(text(),'Show Hidden')]/parent::div/parent::div
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*/div[@role='presentation']//*/label[text()='Show Hidden']/parent::div/parent::div
    Set Focus To Element   xpath=//*/div[@role='presentation']//*/label[text()='Show Hidden']/parent::div/parent::div
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*/div[@role='presentation']//*/label[text()='Show Hidden']/parent::div/parent::div
    Sleep  ${DELAY}

Show All
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="graphExplorerControl"]/div//*[text()='Simplify Graph']/parent::div//*/button
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=/html/body//*/span[@label='Show all']
    Sleep  ${DELAY}

Zoom Out Map
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="graph-zoom-buttons"]/button[@ui-log-desc='Zoom out button pressed']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="graph-zoom-buttons"]/button[@ui-log-desc='Zoom out button pressed']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="graph-zoom-buttons"]/button[@ui-log-desc='Zoom out button pressed']
    Sleep   ${DELAY}

Click Search
    Double Click Element    xpath=//*[@id="searchAutoComplete"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="appbar-wrapper"]/div/div[1]/div[2]/div[2]/button[1]/div/*
    Sleep   ${DELAY}

# Neighbours
Check If Neighbours Of Single Node Are Returned
     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element   xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
     ${node_asset_type}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
     ${node_asset_name}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
#     Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Link    xpath=//*[@id="list-bulk-update-control"]//a[text()='Neighbours']
     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Button    xpath=/html/body/div[11]/div/div[1]/div/div/div[2]/button[2]
     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="nodesTable"]/tbody/tr
     ${child_list}   Fetch Relationships    ${node_asset_name}
     Go To Map View
     Show Hidden
     Show All
     Check For Neighbouring Nodes     ${node_asset_name}    ${child_list}

Check If Neighbours Of Multiple Nodes Are Returned
    Wait For Table to Populate
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="nodesTable"]/tbody/tr[2]
    ${node_asset_type_1}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
    ${node_asset_name_1}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    ${node_asset_type_2}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[5]
    ${node_asset_name_2}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[6]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[1]/input
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Link    xpath=//*[@id="list-bulk-update-control"]//a[text()='Neighbours']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Button    xpath=/html/body/div[11]/div/div[1]/div/div/div[2]/button[2]
    Wait For Table to Populate
    ${list_node_1}     Fetch Relationships     ${node_asset_name_1}
    ${list_node_2}     Fetch Relationships     ${node_asset_name_2}
    Go To Map View
    Show Hidden
    Show All
    Check For Neighbouring Nodes    ${node_asset_name_1}    ${list_node_1}
    Check For Neighbouring Nodes    ${node_asset_name_2}    ${list_node_2}

Check For Neighbouring Nodes
    [Arguments]     ${parent_node}   ${child_node_list}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element   //*[@id="graph-view-svg"]//*[text()="${parent_node}"]
    ${node_coordinate}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${parent_node}"]/parent::*    transform
    ${node_coordinate}  Fetch From Left    ${node_coordinate}    )
    ${node_coordinate}  Fetch From Right    ${node_coordinate}   (
    :FOR    ${element}   In     @{child_node_list}
    \   ${child_coordinate}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${element}"]/parent::*    transform
    \   ${child_coordinate}  Fetch From Left    ${child_coordinate}    )
    \   ${child_coordinate}  Fetch From Right    ${child_coordinate}   (
    \   ${count1} =     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Count   xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${child_coordinate},${node_coordinate}')]
    \   Run Keyword If     ${count1}==0    Page Should Contain Element    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${node_coordinate},${child_coordinate}')]

Fetch Relationships
    [Arguments]     ${business_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodesTable"]/tbody/tr//*[text()='${business_name}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="nodeNeighbours"]//*/a[text()="Relationships"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodeNeighbours"]//*/a[text()="Relationships"]
    Sleep   ${DELAY}
    @{list}     Create List
    ${Rows}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Count    //*[@id="nodeNeighbours"]/div/div[contains(@class,'node-neighbours')]/div[1]
    :FOR    ${row}    IN RANGE    0    ${Rows}
    \  ${col_name}   Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    //*[@id="nodeNeighbours"]/div/div[${row}+2]/div[1]
    \  ${col_name}    Run Keyword If    '${col_name}'=='This'   Get Text    //*[@id="nodeNeighbours"]/div/div[${row}+2]/div[2]   ELSE   Set Variable  ${col_name}
    \   Append To List     ${list}      ${col_name}
    Wait Until Element Is Visible  xpath=//*[@id="Main-section"]
    Close Enrich Page
#    Mouse Over    xpath=//*[@class="action-bar-close"]
#    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@class="action-bar-close"]
    [Return]     ${list}


Return Marked Favourite
    ${business_name}  Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text  //*[@id="nodesTable"]/tbody/tr[1]/td[6]
    [Return]  ${business_name}

Mark Favourite
    [Arguments]     ${row_num}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="nodesTable"]/tbody/tr[${row_num}]/td[2]/img
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="nodesTable"]/tbody/tr[${row_num}]/td[2]/img

Check If Node Added To Favourites
    [Arguments]  ${business_name}
    Go To Favourites
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element  xpath=//*[@id="subscriptionwrapper"]//*[text()='${business_name}']

Execute Favourite
    [Arguments]  ${business_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="subscriptionwrapper"]//*[text()='${business_name}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="details-view"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="details-view"]//*/input[@value='${business_name}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="details-view"]//*/a[@title='Subscribe']

Remove From Favourites
     [Arguments]    ${business_name}
     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="subscriptionwrapper"]//*[text()='${business_name}']/parent::div/parent::div/span[@class='subscribe']/img

# Download import manager files
Download Import-Manager Files From The First Job
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
#    ${date}    Get Current Date    result_format=epoch
##    ${date_epoch_aux}    Convert To Number    ${date}    precision=1
#    ${date_epoch}    Convert To Number    ${date}    precision=0
#    ${date_epoch_string}    Convert To String    ${date_epoch}
#    ${file_name}    Get Substring    ${date_epoch_string}    0    8
    ${file_name}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Attribute   xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]/td    title
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[text()="Download"]/parent::div
#    Wait Until Created    ${DOWNLOAD_LOCATION}/${file_name}[0-9][0-9].zip
#    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   File Should Exist    ${DOWNLOAD_LOCATION}/${file_name}[0-9][0-9].zip
#    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Remove File      ${DOWNLOAD_LOCATION}/${file_name}[0-9][0-9].zip
    Wait Until Created    ${DOWNLOAD_LOCATION}/import-${file_name}.zip
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   File Should Exist    ${DOWNLOAD_LOCATION}/import-${file_name}.zip
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Remove File      ${DOWNLOAD_LOCATION}/import-${file_name}.zip

Import Data Assets To Alex
    [Arguments]     ${list_of_files}
    ${input_files}    Set Variable    ${IMPORT_TEST_DIR}@{list_of_files}[0]
    ${list_length}    Get Length    ${list_of_files}
    :FOR    ${index}    IN RANGE    1    ${list_length}
    \    ${input_files}    Catenate    SEPARATOR= \n     ${input_files}    ${IMPORT_TEST_DIR}@{list_of_files}[${index}]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     //*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[2]/div/label/div/div/input
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Choose Files    //*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[2]/div/label/div/div/input    ${input_files}
    Sleep   ${DELAY}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[contains(text(),'Promote')]
    Sleep   ${DELAY}
    :FOR    ${i}    IN RANGE   10
    \   Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[contains(text(),'Refresh')]
    \   ${status}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]/td[12]
    \   Exit For Loop If    '${status}'=='Approved'
    \   Sleep  ${IMPORT_DELAY}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}     Page Should Contain Element     xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]/td[12][text()='Approved']

Import Data Assets
    [Arguments]     ${list_of_files}
    ${input_files}    Set Variable    ${IMPORT_DIR}@{LIST_OF_FILES}[0]
    ${list_length}    Get Length    ${LIST_OF_FILES}
    :FOR    ${index}    IN RANGE    1    ${list_length}
    \    ${input_files}    Catenate    SEPARATOR= \n     ${input_files}    ${IMPORT_DIR}@{LIST_OF_FILES}[${index}]
#    Wait Until Page Contains Element    //*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[2]/div/label/div/div/input
#    Choose Files    //*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[2]/div/label/div/div/input    ${input_files}
#    Set Selenium Speed  3min
#    Wait Until Page Contains Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr/td[contains(text(),'Processing')]
#    Sleep   2h
#    Wait Until Page Contains Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr/td[contains(text(),'Pending')]
#    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
#    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[contains(text(),'Promote')]
#    Sleep   20s
#    :FOR    ${i}    IN RANGE    3
#    \    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[contains(text(),'Refresh')]


Verify Dataset Upload
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[text()="Show Stats"]/parent::div
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=/html/body/div[9]/div/div[1]/div/div//*[@class="pending-stats"]
    ${number_of_assets}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=/html/body/div[9]/div/div[1]/div/div//*[@class="pending-stats"]/div[1]
    Should Be Equal As Strings    ${number_of_assets}    3
    Go To Home Page
    Expand Filter     ${TECHNOLOGY}
    Click Explore Tab
#    Wait Until Page Contains Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='Technology']/parent::div/following-sibling::div//label[contains(text(),'SampleTechnology')]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='Technology']/parent::div/following-sibling::div//label[contains(text(),'SampleTechnology')]/parent::div/parent::div
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    //*[@id="nodesTable"]/tbody/tr[1]//td[12][text()="SampleTechnology"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     //*[@id="nodesTable"]/tbody/tr[2]//td[12][text()="SampleTechnology"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    //*[@id="nodesTable"]/tbody/tr[3]//td[12][text()="SampleTechnology"]
    Click Capture Tab
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
    Sleep   ${SHORT_DELAY}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[contains(text(),'Rollback')]
    Sleep   ${DELAY}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[contains(text(),'Refresh')]

#The keyword looks for the argument to be present in one of the columns in the table.
#If not then it opens the enrich page and looks for all the property values for the searched string.
Check If Nodes Returned Have Searched Property
    [Arguments]    ${search_term}
    ${search_term} =  Convert To Uppercase  ${search_term}
    Wait For Table to Populate
    ${present} =  Get Element Count     xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[contains(translate(text(),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'), '${search_term}')]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="nodesTable"]/tbody/tr/td[5]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="nodeDetailForm"]//*[@id='Main-section']
    ${present} =  Run Keyword If  ${present}==0    Get Element Count  xpath=//*[@id="nodeDetail"]//*/input[contains(translate(@value,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'), '${search_term}')]
    ${present} =  Run Keyword If  ${present}==0    Get Element Count   xpath=//*[@id="nodeDetail"]//*/textarea[contains(translate(@value,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'), '${search_term}')]
    Run Keyword If  ${present}==0   Wait Until Page Contains Element  xpath=//*[@id="nodeNeighbours"]//*[contains(translate(text(),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'), '${search_term}')]
    #Checking dropdowns remaining
    Close Enrich Page
#    Wait Until Element Is Visible  xpath=//*[@class="action-bar-close"]
#    Mouse Over    xpath=//*[@class="action-bar-close"]
#    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@class="action-bar-close"]

Check If Property Contains
    [Arguments]     ${search_term}  ${col_no}
    ${search_term} =    Convert To Uppercase    ${search_term}
    ${Rows}    Get Element Count    //*[@id="nodesTable"]/tbody/tr
    :FOR    ${row}    IN RANGE    1    ${Rows}
    \    ${business_name} =  GET TEXT  xpath=//*[@id="nodesTable"]/tbody/tr[${row}]/td[${col_no}]
    \    ${business_name} =  Convert To Uppercase   ${business_name}
    \    Should Contain  ${business_name}  ${search_term}

Go To Advanced Search
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="appbar-wrapper"]/div/div[1]/div[2]/div[2]/button[2]

Populate Values In Asset Property Advanced Search
    [Arguments]    ${asset_property}  ${asset_property_val}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text    xpath=//html/body/div[23]//*[text()="Select Asset Property"]/parent::div/child::input  ${asset_property}
    Press Key     xpath=//html/body/div[23]//*[text()="Select Asset Property"]/parent::div/child::input   \uE015
    Press Key     xpath=//html/body/div[23]//*[text()="Select Asset Property"]/parent::div/child::input   \uE007
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text    xpath=//html/body/div[23]//*[text()="Asset Property Value"]/parent::div/child::input   ${asset_property_val}
    Press Key     xpath=//html/body/div[23]//*[text()="Select Asset Property"]/parent::div/child::input   \uE007

Add Relationship Type In Advanced Search
    [Arguments]     ${rel_type}    ${rel_type_val}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//html/body/div[23]//*[text()="Select Relationship Property"]/parent::div
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element      xpath=//html/body/div[23]//*[text()="Select Relationship Property"]/parent::div
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     //*[@id="${rel_type}"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element       //*[@id="${rel_type}"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//html/body/div[23]//*[text()="Relationship Property Value"]/parent::div/input
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text    xpath=//html/body/div[23]//*[text()="Relationship Property Value"]/parent::div/input      ${rel_type_val}

Save Search
    [Arguments]   ${saved_as}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=/html/body/div[23]//*[text()="Save Search"]/parent::div
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text  xpath=/html/body/div[24]//*[text()="Search Name"]/parent::div/child::input  ${saved_as}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=/html/body/div[24]/div/div[1]/div/div/div[2]/button[2]

Check For Saved Search
    [Arguments]   ${saved_as}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}  Page Should Contain Element  xpath=//*[@id="favouriteQuerieswrapper"]//*[text()="${saved_as}"]

Execute Search
    [Arguments]   ${saved_as}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element   xpath=//*[@id="favouriteQuerieswrapper"]//*[text()="${saved_as}"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="favouriteQuerieswrapper"]//*[text()="${saved_as}"]
    Sleep   ${DELAY}


Delete Saved Search
    [Arguments]   ${saved_as}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="favouriteQuerieswrapper"]//*[text()="${saved_as}"]/parent::div/parent::div/div/i

# Keep-only on single selection
Check If Only Nodes Of Subcontext Remain
    Wait For Table to Populate
    ${num_records}      Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Count   xpath=//*[@id="nodesTable"]/tbody/tr
    Run Keyword If    ${num_records}>2     Select First Two Nodes   ELSE    Fail
    ${node_1}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    ${node_2}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[6]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="tab-list-switchs"]//a[text()="Keep Only"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="nodesTable"]/tbody/tr[1]
    ${num_rows_returned}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Count   xpath=//*[@id="nodesTable"]/tbody/tr
    Should Be Equal As Integers   ${num_rows_returned}    2
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element      xpath=//*[@id="nodesTable"]/tbody/tr/*[text()='${node_1}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element      xpath=//*[@id="nodesTable"]/tbody/tr/*[text()='${node_2}']


# Hide on node selection
Check If The Number Of Nodes Reduces
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="nodesTable"]/tbody/tr[1]
    ${num_records}    Get Rows Returned
    Run Keyword If    ${num_records}>2     Select First Two Nodes   ELSE    Fail
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element     xpath=//*[@id="tab-list-switchs"]//a[text()="Hide"]
    ${num_records}    Evaluate  ${num_records}-2
    ${num_records_returned}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}   ${SHORT_DELAY}   Get Rows Returned
    Should Be Equal As Integers    ${num_records}   ${num_records_returned}

Check Impact Of Data Asset
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="tab-list-switchs"]//a[text()="Impact"]

Check Lineage Of Data Asset
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="tab-list-switchs"]//a[text()="Lineage"]

Check End-to-End Of Data Asset
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="tab-list-switchs"]//a[text()="End-to-End"]

Get All Asset Types
    @{asset_types}     Create List
    ${num_of_assets}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Count  xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='Type of Asset']/parent::div/parent::div//*/label[text()!='All']
    :For    ${row}  In Range   0    ${num_of_assets}
#     \  ${asset_name}       Get Text    xpath=//*[@id="appbar-wrapper"]/*//div[text()='Type of Asset']/parent::div/parent::div//*/label[text()!='All']
     \  ${asset_name}       Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="appbar-wrapper"]/*//div[text()='Type of Asset']/parent::div/parent::div/div/div[${row+2}]/*//label
     \  ${asset_name}       Fetch From Left     ${asset_name}   (
     \  ${asset_name}       Strip String     ${asset_name}
     \   Append To List     ${asset_types}  ${asset_name}
     [Return]   ${asset_types}

Get All Nodes On Map
     @{nodes}       Create List
     ${num_of nodes}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Count   //*[@id="graph-view-svg"]//*/*[@class='node']/child::*
     :For    ${row}  In Range   0    ${num_of_nodes}
#     \  Wait Until Page Contains Element    //*[@id="graph-view-svg"]//*/*[@class='node']/*[${row}+1]/*[@class='circle-text']
     \  ${node}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    //*[@id="graph-view-svg"]//*/*[@class='node']/*[${row}+1]/*[@class='circle-text']
     \  Append To List      ${nodes}    ${node}
     [Return]   ${nodes}


Check For Nodes
    ${asset_types}  Get All Asset Types
    ${nodes}    Get All Nodes On Map
    :For    ${node}    IN  @{nodes}
#    \   Wait Until Page Contains Element     //*[@id="graph-view-svg"]//*[text()='${node}']
    \   ${node_coordinate}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Attribute    //*[@id="graph-view-svg"]//*[text()='${node}']/parent::*    transform
    \   ${node_coordinate}  Fetch From Left    ${node_coordinate}    )
    \   ${node_coordinate}  Fetch From Right    ${node_coordinate}   (
    \   Run Keyword If  '${node}'!='Column' and '${node}'!='User'    Wait Until Page Contains Element   xpath=//*[@id="graph-view-svg"]//*[contains(@d,'${node_coordinate}')]


Map Should Only Contain
    [Arguments]     ${filter1}  ${filter2}
#    Wait Until Page Contains Element    //*[@id="graph-view-svg"]//*/*[@class='node']
    ${num_of_nodes}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Count   //*[@id="graph-view-svg"]//*/*[@class='node']/child::*
    :For    ${row}  In Range   0    ${num_of_nodes}
    \  ${node}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    //*[@id="graph-view-svg"]//*/*[@class='node']/*[${row}+1]/*[@class='circle-text']
    \  Run Keyword If   '${node}'!='${filter1}'     Should Be Equal   ${node}   ${filter2}

Check For Type Ahead
    [Arguments]     ${search_term}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text    id:searchAutoComplete    ${search_term}
    Press Key     xpath=//*[@id="searchAutoComplete"]   \uE015
    ${search_term}     Convert To Lowercase    ${search_term}
    ${num_of_suggestions}   Get Element Count  //div[@role="presentation"]//*/div/span
    :For    ${row}  In Range    0   ${num_of_suggestions}
    \   Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//div[@role="presentation"]//*/div[${row}+1]/span//*/div[contains(text(),${search_term})]

Check For Results On Last Page
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="nodesTable"]/tbody/tr[1]
    ${results_size}     Get Rows Returned
    ${results_size}     Convert To Number   ${results_size}
    ${num_of_pages}      Evaluate    ${results_size}/15
    ${temp}      Convert To Integer   ${num_of_pages}
    ${num_of_pages} =   Run Keyword If     ${temp}!=${num_of_pages}     Evaluate    ${temp}+1   ELSE    Set Variable    ${temp}
#    Wait Until Page Contains Element    xpath=//*[@id="nodesTable_paginate"]/ul/li//*[text()='${num_of_pages}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="nodesTable_paginate"]/ul/li//*[text()='${num_of_pages}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="nodesTable"]/tbody/tr[1]
    ${rows}     Get Element Count      xpath=//*[@id="nodesTable"]/tbody/tr
    ${status}       Evaluate    ${rows}>0
    Run Keyword If     ${status}!=True      Fail

Lock Context
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}  Page Should Contain Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="HomeAppBarSearchResult"]/button
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}  Page Should Contain Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]

Get Number Of Records For A Facet Filter
    [Arguments]     ${filter_type}   ${filter}
#    Wait Until Page Contains Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]//*/div[text()='${filter_type}']/parent::div/following-sibling::div//label[contains(text(),'${filter}')]
    ${num}       Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    //*[@id="appbar-wrapper"]/div/div[3]//*/div[text()='${filter_type}']/parent::div/following-sibling::div//label[contains(text(),'${filter}')]
    ${num}       Fetch From Right    ${num}   (
    ${num}       Fetch From Left   ${num}   )
    Convert To Integer      ${num}
    [Return]    ${num}

Get Rows Returned
#    Wait Until Page Contains Element    xpath=//*[@id="HomeAppBarSearchResult"]//*/span[@class='resultSize']
    ${result}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="HomeAppBarSearchResult"]//*/span[@class='resultSize']
    @{split}      Split String      ${result}   ${SPACE}
    ${result}     Set Variable   @{split}[0]
    [Return]    ${result}

Open Enrich Page for Data Object
    [Arguments]     ${searched_obj}=${EMPTY}
    ${searched_obj}     Run Keyword If      '${searched_obj}'=='${EMPTY}'     Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]     ELSE    Set Variable    ${searched_obj}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="nodesTable"]/tbody/tr/td[6][text()='${searched_obj}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Element Should Be Visible   xpath=//*[@id="details-view"]
    [Return]    ${searched_obj}

Close Enrich Page
    Wait Until Element Is Visible  xpath=//*[@id="Main-section"]
    Mouse Over    xpath=//*[@class="action-bar-close"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@class="action-bar-close"]

Add New Property To Data Object
    [Arguments]     ${property}     ${property_value}
    Sleep   ${SHORT_DELAY}
    Mouse Over      xpath=//*[@id="Other-section"]/legend/a
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="Other-section"]/legend/a
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="Other-section"]//*/a[text()='Add Property']
    Mouse Over    xpath=//*[@id="Other-section"]//*/a[text()='Add Property']
    Sleep   ${SHORT_DELAY}
    Set Focus To Element    xpath=//*[@id="Other-section"]//*/a[text()='Add Property']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="Other-section"]//*/a[text()='Add Property']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="attributeNameInput"]
    Set Focus To Element    xpath=//*[@id="attributeNameInput"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text      xpath=//*[@id="attributeNameInput"]     ${property}
    Set Focus To Element    xpath=//*[@id="attributeValueInput"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text      xpath=//*[@id="attributeValueInput"]    ${property_value}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="tab-details_ModalAddAttribute"]/a[text()='Submit']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="custom-test"]//*/label[@for='${property}']
    Save Property

Attribute Should Be Added
    [Arguments]     ${property}     ${property_value}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="Other-section"]/legend/a
    Mouse Over      xpath=//*[@id="Other-section"]/legend/a
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="Other-section"]/legend/a
    Mouse Over      xpath=//*[@id="Other-section"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="custom-${property}"]//*/input[@value='${property_value}']
    Set Focus To Element  xpath=//*[@id="custom-${property}"]//*/input[@value='${property_value}']

Delete Attribute
    [Arguments]     ${property}     ${property_value}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="Other-container"]//*[@id="remove-${property}"]
    Mouse Over      xpath=//*[@id="Other-container"]//*[@id="remove-${property}"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="Other-container"]//*[@id="remove-${property}"]
    Handle Alert
    Save Property

Update Property
    [Arguments]     ${property}    ${property_val}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id='Main-container']//*/textarea[@id='${property}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text  xpath=//*[@id='Main-container']//*/textarea[@id='${property}']    '${property_val}'
    Save Property

Save Property
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="save"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="save"]
    Sleep   ${DELAY}
    Close Enrich Page
#    Mouse Over    xpath=//*[@class="action-bar-close"]
#    Set Focus To Element  xpath=//*[@class="action-bar-close"]
#    Sleep   ${SHORT_DELAY}
#    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@class="action-bar-close"]

Check If Property Updated
    [Arguments]     ${property}    ${property_val}
#    Wait Until Page Contains Element    xpath=//*[@id='Main-container']//*/textarea[@id='${property}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Element Should Contain      xpath=//*[@id='Main-container']//*/textarea[@id='DESCR']      '${property_val}'
    Delete Property     ${property}

Delete Property
    [Arguments]     ${property}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id='Main-container']//*/textarea[@id='${property}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text  xpath=//*[@id='Main-container']//*/textarea[@id='${property}']     ${EMPTY}
    Save Property

Mark Assets As Favourite
    [Arguments]     ${num_col}
    @{business_names}       Create List
    :FOR    ${row}    In Range    0   ${num_col}
    \   Mark Favourite      ${row+1}
    \   ${business_name}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[${row+1}]/td[6]
    \   Append To List      ${business_names}   ${business_name}
    [Return]    ${business_names}

Sort By Favourites
    Wait For Table to Populate
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="nodesTable_wrapper"]/div[1]/div[1]/div/table/thead/tr/th[2]
    Sleep   ${DELAY}

Check If Marked Favourite
    [Arguments]     ${business_names}   ${num}
    :For    ${row}     In Range     0   ${num}
    \   ${business_name}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[${row+1}]/td[6]
    \   Mark Favourite      ${row+1}
    \   List Should Contain Value   ${business_names}   ${business_name}

Export Results
    [Arguments]    ${type_of_export}    ${file_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="catalogExportMenu"]//*/button
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*/div[@role='presentation']//*/div[text()=${type_of_export}]
    Enable Download In Headless Chrome      ${DOWNLOAD_LOCATION}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="tab-properties-node-submit"]
    Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}   File Should Exist    ${DOWNLOAD_LOCATION}/${file_name}.csv

Check If File Contains All Rows
    [Arguments]     ${file_name}    ${num_rows_expected}
    ${file_content} =   Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get File     ${DOWNLOAD_LOCATION}/${file_name}.csv
    @{lines} =    Split To Lines    ${file_content}
    Remove From List    ${lines}    0
    ${rows_written}     Get Length  ${lines}
    Should Be Equal As Integers    ${rows_written}  ${num_rows_expected}

Go To Settings
    [Arguments]     ${option}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="appbar-wrapper"]/div/div[1]/div[3]/div[1]/div/button
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@role="presentation"]//*/div[text()='${option}']/parent::div

Add User
    [Arguments]     ${user_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@data-reveal-id="ModalAddUser"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="ModalAddUser"]//*/input[@name='firstName']   ${USER_FIRST_NAME}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="ModalAddUser"]//*/input[@name='lastName']    ${USER_LAST_NAME}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="ModalAddUser"]//*/input[@name='userName']    ${user_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="ModalAddUser"]//*/input[@name='password']    ${USER_PASSWORD}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="ModalAddUser"]//*/input[@name='passwordConfirm']    ${USER_PASSWORD}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="ModalAddUser"]//*/input[@name='email']       ${USER_EMAIL}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="ModalAddUser"]//*/input[@name='phone']       ${USER_CONTACT}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="registration_button"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="userTable"]/tbody/tr/td[1][text()='${user_name}']

Password Reset Page Should Open
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="temporaryPasswordResetForm"]

Reset Password And Login
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="temporaryPasswordResetForm"]//*/input[@name="temporaryPassword"]     ${USER_PASSWORD}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="temporaryPasswordResetForm"]//*/input[@name="password"]              ${USER_PASSWORD}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="temporaryPasswordResetForm"]//*/input[@name="passwordConfirm"]       ${USER_PASSWORD}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="temporaryPasswordReset_button"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Location Should Be    ${ALEX_HOME}

Add Role
    [Arguments]     ${role}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="userAdminwrapper"]//*/a[text()="Roles Management"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@data-reveal-id="ModalAddUserGroup"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="ModalAddUserGroup"]//*/input[@name='groupName']      ${role}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="ModalAddUserGroup"]//*/input[@name='groupDescription']   ${ROLE_DESCR}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="addUserGroup_wrapper"]/form//*/h5[text()="Security Components"]/parent::div//*/input[@propname="Read Node"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="addUserGroup_wrapper"]/form//*/h5[text()="Security Components"]/parent::div//*/input[@propname="Read Relationship"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="addUserGroup_wrapper"]/form//*/h5[text()="Security Components"]/parent::div//*/input[@propname="Export"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="addUserGroup_button"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="groupTable"]/tbody/tr/td[text()='${role}']

Assign Role To User
    [Arguments]     ${user}     ${role}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="userAdminwrapper"]//*/a[text()="Roles Management"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="groupTable"]/tbody/tr/td[text()='${role}']/parent::tr/td/a[text()='Members']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text     xpath=//*[@id="manageUsers_wrapper"]/div[1]/input       ${user}(User)
#    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="serach-user-typeahead"]/ul/li[@name='${user}(User)']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="add_user"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="userGroupsTable"]/tbody/tr/td[3][text()='${user}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="ModalManageUsers"]/a[text()='×']

Check For Rights
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Not Contain Element    xpath=//button[.//text() = 'Capture']
    Click Explore Tab
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="nodesTable"]/tbody/tr[1]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element    xpath=//*[@id="msg-box"]//*[contains(text(),'Error')]

Delete User
    [Arguments]     ${user}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="userTable"]/tbody/tr/td[1][text()='${user}']/parent::tr/td/a[text()='Delete']
    Handle Alert
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Not Contain Element    xpath=//*[@id="userTable"]/tbody/tr/td[1][text()='${user}']


Delete Role
    [Arguments]     ${role}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="userAdminwrapper"]//*/a[text()="Roles Management"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="groupTable"]/tbody/tr/td[text()='${role}']/parent::tr/td/a[text()='Delete']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Not Contain Element    xpath=//*[@id="groupTable"]/tbody/tr/td[text()='${role}']

Expand Relationships
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="nodeNeighbours"]//*/a[text()="Relationships"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodeNeighbours"]//*/a[text()="Relationships"]

Get Impact Nodes
    [Arguments]     ${node}
    @{list}     Create List
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath= //*[@id="nodeNeighbours"]/div/div[contains(@class,'node-neighbours')][1]
    ${Rows}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Count    //*[@id="nodeNeighbours"]/div/div[contains(@class,'node-neighbours')]
    :FOR    ${row}    IN RANGE    0    ${Rows}
    \  ${col_name}   Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    //*[@id="nodeNeighbours"]/div/div[${row}+2]/div[2]
    \  Run Keyword If    '${col_name}'!='This'      Append To List     ${list}      ${col_name}
    [Return]    ${list}

Get Lineage Nodes
    [Arguments]     ${node}
    @{list}     Create List
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath= //*[@id="nodeNeighbours"]/div/div[contains(@class,'node-neighbours')][1]
    ${Rows}     Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Count    //*[@id="nodeNeighbours"]/div/div[contains(@class,'node-neighbours')]
    :FOR    ${row}    IN RANGE    0    ${Rows}
    \  ${col_name}   Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Text    //*[@id="nodeNeighbours"]/div/div[${row}+2]/div[1]
    \  Run Keyword If    '${col_name}'!='This'      Append To List     ${list}      ${col_name}
    [Return]    ${list}

Get Asset Type Of Nodes
    [Arguments]     ${node_list}    ${asset_type_list}
    :For    ${node}     IN    @{node_list}
    \   Search For Assets   BUS_NAME="${node}"
    \   ${asset_type_list}  Get Asset Type Of Node    ${node}   ${asset_type_list}
    ${asset_type_list}      Remove Duplicates      ${asset_type_list}
    [Return]    ${asset_type_list}

Get Asset Type Of Node
    [Arguments]     ${node}    ${list}
    ${num_nodes}        Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Element Count   xpath=//*[@id="nodesTable"]/tbody/tr/td[text()='${node}']
    :FOR    ${row}  IN RANGE    0   ${num_nodes}
    \   ${asset_type}       Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text   xpath=//*[@id="nodesTable"]/tbody/tr[${row}+1]/td[5]
    \   Append To List      ${list}     ${asset_type}
    [Return]    ${list}

Check If Impact Results For Node Is Returned
    [Arguments]    ${parent_node}  ${child_nodes_list}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element   //*[@id="graph-view-svg"]//*[text()="${parent_node}"]
    ${node_coordinate}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${parent_node}"]/parent::*    transform
    ${node_coordinate_fin}  Fetch From Left    ${node_coordinate}    )
    ${node_coordinate_fin}  Fetch From Right    ${node_coordinate_fin}   (
    :FOR    ${element}   In     @{child_nodes_list}
    \   ${child_coordinate}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${element}"]/parent::*    transform
    \   ${child_coordinate} =     Run Keyword If     '${child_coordinate}'=='${node_coordinate}'      Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${element}"]/parent::*/following-sibling::*//*[text()="${element}"]/parent::*    transform      ELSE    Set Variable    ${child_coordinate}
    \   ${child_coordinate}  Fetch From Left    ${child_coordinate}    )
    \   ${child_coordinate}  Fetch From Right    ${child_coordinate}   (
    \   Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}    Page Should Contain Element    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${node_coordinate_fin},${child_coordinate}')]

Check If Lineage Results For Node Is Returned
    [Arguments]    ${parent_node}  ${child_nodes_list}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element   //*[@id="graph-view-svg"]//*[text()="${parent_node}"]
    ${node_coordinate}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${parent_node}"]/parent::*    transform
    ${node_coordinate_fin}  Fetch From Left    ${node_coordinate}    )
    ${node_coordinate_fin}  Fetch From Right    ${node_coordinate_fin}   (
    :FOR    ${element}   In     @{child_nodes_list}
    \   ${child_coordinate}    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${element}"]/parent::*    transform
    \   ${child_coordinate} =     Run Keyword If     '${child_coordinate}'=='${node_coordinate}'      Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${element}"]/parent::*/following-sibling::*//*[text()="${element}"]/parent::*    transform      ELSE    Set Variable    ${child_coordinate}
    \   ${child_coordinate}  Fetch From Left    ${child_coordinate}    )
    \   ${child_coordinate}  Fetch From Right    ${child_coordinate}   (
    \   Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}    Page Should Contain Element    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${child_coordinate},${node_coordinate_fin}')]

Go To Reporting
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="discoverReportingBtn"]

Add New Report
    [Arguments]     ${report_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="reporting"]//*/label[text()='Add new Report']/parent::div/img
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text  xpath=//*/div/h5[text()='General Information']/following-sibling::div/input[@name='title']     ${report_name}

Add Mandatory Details To Report
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text  xpath=//*/div/h5[text()='General Information']/following-sibling::div/textarea[@name='description']    This is a automated test report
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Input Text  xpath=//*[@id="reporting"]//*/input[@placeholder='Search']     Business
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="buildReportContainerLeftPanelProps"]/ul/li[text()='Business Name']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="reporting"]//*/span[text()='Properties']/following-sibling::img

Save Report
   [Arguments]      ${report_name}
   Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element  xpath=//*[@id="reporting"]//*/a[text()='Save']
   Handle Alert
   Wait Until Keyword Succeeds    ${RETRY_INT_LOGIN}  ${SHORT_DELAY}    Page Should Contain Element    xpath=//*[@id="reporting"]//*/h5[text()='${report_name}']
   Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   xpath=//*[@id="reporting"]//*/a[text()='Go Back']

Check If Report Added
    [Arguments]     ${report_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element   //*[@id="reportsTable_wrapper"]//*/td[text()='${report_name}']

Open Report
   [Arguments]     ${report_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element   //*[@id="reportsTable_wrapper"]//*/td[text()='${report_name}']/parent::tr
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element  //*[@id="reporting"]//*/h5[text()='${report_name}']

Check For Contents Of Report
    [Arguments]     ${report_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element  //*[@id="reporting"]//*/h5[text()='${report_name}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element  //*[@id="reportViewTable_wrapper"]/div[2]/div[1]//*/th[text()='Business Name']

Action Report
    [Arguments]     ${action}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element      //*[@id="reporting"]//*/span[text()='Actions']/following-sibling::a
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element      //*[@id="reporting"]//*/ul/li[text()='${action}']
    Handle Alert

Check If Report Deleted
    [Arguments]     ${report_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Not Contain Element   //*[@id="reportsTable_wrapper"]//*/td[text()='${report_name}']

Search Endurance Test
    [Arguments]     ${search_term}
    Click Explore Tab
    Search For Assets   BUS_NAME='${search_term}'
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element      xpath=//*[@id="nodesTable"]/tbody/tr/td[6][text()='${search_term}']

Expand Workflow
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="Workflow-section"]/legend/a
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="Workflow-section"]/legend/a

Populate Workflow Section
    [Arguments]     ${type}=Decommissioning     ${status}=Enrich - Under Review
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="WF_TYPE"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="WF_TYPE"]/option[text()='${type}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="WF_STATUS"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="WF_STATUS"]/option[text()='${status}']
    Save Property

Go To Workflow Dashboard
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="dashboardWorkflowBtn"]
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="dashboard-mid-table"]

Check If Workflow Present
    [Arguments]       ${asset}      ${status}=Under Review
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element      xpath=//*[@id="dashboard-mid-table"]//*/td[2]/a[text()='${status}']
    ${num}      Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text      xpath=//*[@id="dashboard-mid-table"]//*/td[2]/a[text()='Under Review']/following-sibling::span
    ${num}      Convert To Integer     ${num}
    Should Be Equal As Integers     ${num}  1
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="dashboard-mid-table"]//*/td[2]/a[text()='Under Review']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element     xpath=//*[@id="nodesTable"]/tbody/tr/td[6][text()='${asset}']

Mark Workflow Completed
    [Arguments]     ${asset}
    Open Enrich Page for Data Object    ${asset}
    Expand Workflow
    Populate Workflow Section       status=Enrich - Completed

Workflow Should Not Be Present
    [Arguments]       ${asset}      ${status}=Under Review
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Page Should Contain Element      xpath=//*[@id="dashboard-mid-table"]//*/td[2]/a[text()='${status}']
    ${num}      Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}      Get Text      xpath=//*[@id="dashboard-mid-table"]//*/td[2]/a[text()='Under Review']/following-sibling::span
    ${num}      Convert To Integer     ${num}
    Should Be Equal As Integers     ${num}  0

Sort On Column
    [Arguments]     ${col_name}
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Mouse Over       xpath=//*[@id="nodesTable_wrapper"]//*/table/thead/tr/th[text()='${col_name}']
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Click Element    xpath=//*[@id="nodesTable_wrapper"]//*/table/thead/tr/th[text()='${col_name}']

Wait For Table to Populate
    Wait Until Keyword Succeeds    ${RETRY_INT_OP}  ${SHORT_DELAY}   Wait Until Element Is Visible    xpath=//*[@id="nodesTable"]/tbody/tr[1]