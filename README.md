# Commands to be executed for running the tests in parallel


 ###   pabot --processes 8 -d path_to_report_dir pabot/test_suite/
    


    1.  --processes :  It accepts the number of  test suites you want to run in parallel
    2. path_to_report_dir : It is the path where the test reports and screenshots should get generated. A separate folder is recommended.